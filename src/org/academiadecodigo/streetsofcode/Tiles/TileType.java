package org.academiadecodigo.streetsofcode.Tiles;

public enum TileType {
    WALL(0, true, "classroom/parede.png"),
    FLOOR(1, false, "classroom/floor_Sprite.png"),
    TABLE_LEFT(2, true, "classroom/table_Left_Side.png"),
    TABLE_CENTER(3, true, "classroom/table_Center.png"),

    TABLE_MAC(4, true, "classroom/table_center_mac.png"),

    TABLE_RIGHT(5, true, "classroom/table_Right_Side.png"),

    CHAIR_RED(6, true, "classroom/chair_Red.png"),

    CHAIR_BLACK(7, true, "classroom/chair_Black.png"),

    CHAIR_WHITE(8, true, "classroom/chait_White.png"),

    GRASS(9, false, "classroom/relva.png"),

    DOOR_TOP(10, true, "classroom/floor_Door_Top.png"),

    DOOR_DOWN(11, true, "classroom/floor_Door_Down.png"),

    DOOR_TOP_RIGHT(12, true, "classroom/floor_Door_Top_Right.png"),

    WINDOW_CENTER(13, true, "classroom/floor_window_center.png"),

    WINDOW_TOP(14, true, "classroom/floor_window_top.png"),

    WINDOW_DOWN(15, true, "classroom/floor_window_down.png"),

    AC_TOP(16, true, "classroom/floor_AC_top.png"),

    AC_DOWN(17, true, "classroom/floor_AC__bot.png"),

    SKATE(18, true, "classroom/floor_Skateboard.png");
    private int type;
    private boolean collision;

    private String file;

    TileType(int type, boolean collision, String file){
        this.type = type;
        this.collision = collision;
        this.file = file;
    }

    public static TileType fromValue(int tileType) {
        for(TileType t : values())
        {
            if (t.getType() == tileType)
                return t;
        }
        return null;
    }

    public int getType() {
        return type;
    }

    public boolean isColliding() {
        return collision;
    }

    public String getFile() {
        return file;
    }

}
