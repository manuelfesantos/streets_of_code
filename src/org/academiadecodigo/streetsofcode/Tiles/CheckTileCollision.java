package org.academiadecodigo.streetsofcode.Tiles;

import org.academiadecodigo.streetsofcode.Fight.HandlerMouse;
import org.academiadecodigo.streetsofcode.Map.Map;
import org.academiadecodigo.streetsofcode.Player.Player;

public class CheckTileCollision {

    private Player player;

    private Tile[][] tiles;
    private Map map;

    private int playerMinX;
    private int tilesMinx;
    private int playerMaxX;
    private int tilesMaxX;
    private int playerMinY;
    private int tilesMaxY;
    private int playerMaxY;
    private int tilesMinY;

    private HandlerMouse mouse;

    public CheckTileCollision(Player player, Map map) {
        this.player = player;
        this.map = map;
        this.tiles = map.tiles;

    }

    public void testMouse(){
        if(playerMinX <= tilesMaxX && playerMinX >= tilesMinx &&
                playerMinY <= tilesMaxY && playerMinY >= tilesMinY){

        }
    }

    public boolean up() {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                int playerMinX = player.playerTile.getX();
                int tilesMinx = tiles[i][j].rectangle.getX();
                int playerMaxX = player.playerTile.getX() + player.playerTile.getWidth();
                int tilesMaxX = tiles[i][j].rectangle.getX() + tiles[i][j].rectangle.getWidth();
                int playerMinY = player.playerTile.getY();
                int tilesMaxY = tiles[i][j].rectangle.getY() + (tiles[i][j].rectangle.getHeight() / 2);
                int tilesMinY = tiles[i][j].rectangle.getY();


                if (tiles[i][j].tileType.isColliding() && (playerMinX <= tilesMaxX && playerMinX >= tilesMinx &&
                        playerMinY <= tilesMaxY && playerMinY >= tilesMinY)) {
                    player.setCurrentTile(tiles[i][j]);
                    return true;

                }
                if (tiles[i][j].tileType.isColliding() && playerMaxX <= tilesMaxX && playerMaxX >= tilesMinx &&
                        playerMinY <= tilesMaxY && playerMinY >= tilesMinY) {
                    player.setCurrentTile(tiles[i][j]);
                    return true;
                }

            }
        }
        return false;
    }

    public boolean down(){

        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {

                updatePositions(tiles[i][j]);


                if (tiles[i][j].tileType.isColliding() && (playerMinX <= tilesMaxX && playerMinX >= tilesMinx) &&
                        (playerMaxY <= tilesMaxY && playerMaxY >= tilesMinY)) {
                    return true;

                } else if (tiles[i][j].tileType.isColliding() && (playerMaxX >= tilesMinx && playerMaxX <= tilesMaxX) &&
                        (playerMaxY <= tilesMaxY && playerMaxY >= tilesMinY)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean left(){

        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {

                updatePositions(tiles[i][j]);


                if (tiles[i][j].tileType.isColliding() && (playerMinX <= tilesMaxX && playerMinX >= tilesMinx) &&
                        (playerMaxY >= tilesMinY && playerMaxY <= tilesMaxY)) {
                    return true;

                } else if (tiles[i][j].tileType.isColliding() && (playerMinX <= tilesMaxX && playerMinX >= tilesMinx) &&
                        (playerMinY <= tilesMaxY - (tiles[i][j].rectangle.getHeight() / 2) && playerMinY >= tilesMinY)) {
                    return true;
                }
            }
        }
        return false;
    }
    public boolean right() {

        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                updatePositions(tiles[i][j]);

                if (tiles[i][j].tileType.isColliding() && (playerMaxX <= tilesMaxX && playerMaxX >= tilesMinx) &&
                        (playerMaxY <= tilesMaxY && playerMaxY >= tilesMinY)) {
                    return true;

                } else if (tiles[i][j].tileType.isColliding() && (playerMaxX <= tilesMaxX && playerMaxX >= tilesMinx) &&
                        (playerMinY <= tilesMaxY - (tiles[i][j].rectangle.getHeight() / 2) && playerMinY >= tilesMinY)) {
                    return true;
                }

            }
        }
        return false;
    }

    public void updatePositions(Tile tile) {
        playerMinX = player.playerTile.getX();
        tilesMinx = tile.rectangle.getX();
        playerMaxX = player.playerTile.getX() + player.playerTile.getWidth();
        tilesMaxX = tile.rectangle.getX() + tile.rectangle.getWidth();
        playerMinY = player.playerTile.getY();
        tilesMaxY = tile.rectangle.getY() + tile.rectangle.getHeight();
        playerMaxY = player.playerTile.getY() + player.playerTile.getHeight();
        tilesMinY = tile.rectangle.getY() ;
    }


}
