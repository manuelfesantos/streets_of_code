package org.academiadecodigo.streetsofcode.Player;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
