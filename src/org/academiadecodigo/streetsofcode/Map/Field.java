package org.academiadecodigo.streetsofcode.Map;

public class Field {

    public static int originalTileSize = 16;
    public static int scale = 2;
    public static int tileSize = originalTileSize * scale; //32 px
    public static int fieldMaxRow = 24;
    public static final int fieldMaxCol = 32;
    public static int fieldHeight = tileSize * fieldMaxRow + 10; // 768px + 10
    public static int fieldWidth = tileSize * fieldMaxCol + 10; // 1024px + 10



}
