package org.academiadecodigo.streetsofcode.Scenes;

import org.academiadecodigo.streetsofcode.Map.Field;
import org.academiadecodigo.streetsofcode.Map.Map;
import org.academiadecodigo.streetsofcode.Player.Player;
import org.academiadecodigo.simplegraphics.graphics.Color;

import java.io.FileNotFoundException;

public class ClassRoomScene extends Scene {

    private Player player1;

    private Player player2;

    private String[][] player1Sprites = {{"images/player_sprites/bri_costas_1.png", "images/player_sprites/bri_costas_2.png", "images/player_sprites/bri_costas_2.png"},
                                         {"images/player_sprites/bri_direita_1.png", "images/player_sprites/bri_direita_2.png", "images/player_sprites/bri_direita_3.png"},
                                        {"images/player_sprites/bri_esquerda_1.png", "images/player_sprites/bri_esquerda_2.png", "images/player_sprites/bri_esquerda_3.png"},
                                         {"images/player_sprites/bri_frente_1.png", "images/player_sprites/bri_frente_2.png", "images/player_sprites/bri_frente_3.png"},};

    private String[][] player2Sprites = {{"images/player_sprites/nuno_Costas1.png", "images/player_sprites/nuno_Costas2.png", "images/player_sprites/nuno_Costas3.png"},
                                        {"images/player_sprites/nuno_direita_1.png", "images/player_sprites/nuno_direita_3.png", "images/player_sprites/nuno_direita_2.png"},
                                        {"images/player_sprites/nuno_esquerda_3.png", "images/player_sprites/nuno_esquerda_1.png", "images/player_sprites/nuno_esquerda_2.png"},
                                         {"images/player_sprites/nuno_frente_1.png", "images/player_sprites/nuno_frente_2.png", "images/player_sprites/nuno_frente_3.png"},};

    private Map map;
    private FightScene fight;

    public ClassRoomScene(SceneManager sceneManager) {
        super(sceneManager);
        fight = (FightScene)getSceneManager().getScenes()[1];
        map = new Map();
        player1 = new Player(map, 2, 2, Color.BLUE, player1Sprites);
        player2 = new Player(map, Field.fieldMaxCol - 3, Field.fieldMaxRow - 3, Color.MAGENTA, player2Sprites);
        player1.setPlayer(player2);
        player2.setPlayer(player1);
    }

    @Override
    public void load() { // lógica inicialização da cena
        try {
            map.loadMap();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }

        /*
        gameState = GameState.MOVE_PLAYERS;
        previousGameState = GameState.MOVE_PLAYERS;

         */

        player1.drawPlayer();
        player2.drawPlayer();

        player1.setKeyboard("player1"); // setKeyboard
        player2.setKeyboard("player2");
        System.out.println("Loaded map & players");

    }

    @Override
    public void update() {
        player1.update();
        player2.update();

        if ((player1.isFighting() || player2.isFighting()) && !fight.fightIsOver) {
            getSceneManager().loadScene(1); // verificar se está em modo fight
            //caso esteja vai fazer load da cena do fight
        }
    }


    @Override
    public void unload() { //delete de elementos visuais
        for(int i = 0; i < map.tiles.length; i++){
            for(int j = 0; j < map.tiles[i].length; j++){
                map.tiles[i][j].rectangle.delete();
            }
        }
        player1.playerPicture.delete();
        player2.playerPicture.delete();

    }

    public void setFight(FightScene fight) {
        this.fight = fight;
    }
}
